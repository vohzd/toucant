export default ({store, redirect}) => {
  if (store.getters.userMeta){
    return redirect("/profile/dashboard");
  }
};
