import getters 				  						from "./rootGetters.js";
import state 				    						from "./rootState.js";

export default {
  hideModal({ commit, dispatch }, modalName){
    commit(`SET_IS_${modalName}_MODAL_SHOWN`, false);
  },
  showModal({ commit, dispatch }, modalName){
    commit(`SET_IS_${modalName}_MODAL_SHOWN`, true);
  },
};
