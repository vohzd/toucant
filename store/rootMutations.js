import state from "./rootState.js";

export default {
  SET_IS_LOADING(state, bool){
    state.isLoading = bool;
  }
};
