import state from "./state.js";

export default {
  allUsers: (state) => state.allUsers,
  authInProgress: (state) => state.authInProgress,
  isLoggedIn: (state) => state.isLoggedIn,
  loggedInUser: (state) => state.loggedInUser,
  loginNotification: (state) => state.loginNotification,
  userMeta: (state) => state.userMeta,
  userOrders: (state) => state.userOrders
};
