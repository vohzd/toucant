export default {
  allUsers: [],
  authInProgress: false,
  isLoggedIn: false,
  loggedInUser: null,
  loginNotification: null,
  userMeta: null,
  userOrders: []
}
