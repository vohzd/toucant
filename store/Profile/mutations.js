import state from "./state.js";

export default {
  IS_AUTHENTICATION_IN_PROGRESS(state, bool){
    state.authInProgress = bool;
  },
  SET_IS_LOGGED_IN(state, bool){
    state.isLoggedIn = bool;
  },
  SET_LOGGED_IN_USER(state, email){
    state.loggedInUser = email;
  },
  SET_LOGIN_NOTIFICATION(state, message){
    state.loginNotification = message;
  },
  SET_USER_META(state, meta){
    state.userMeta = meta;
  },
  SET_ALL_USERS(state, users){
    state.allUsers = users;
  },
  SET_USER_ORDERS(state, orders){
    state.userOrders = orders;
  }
}
