import state 												from "./state.js";
//import uuid                         from "~/assets/js/uuid.js";

export default {
  async checkAuthState({ commit, dispatch }){
    try {
      console.log("checking..")
      commit("SET_IS_LOADING", true);
      let req = await this.$axios.get(`http://localhost:1337/checkAuthState`, { withCredentials: true });
      console.log(req);
      console.log("YOU ARE HERE");
      if (req.status === 200){
        commit("SET_IS_LOADING", false);
        dispatch("setUserMeta", req.data.user);
      }
    }
    catch (e){
      if (e.response.status === 401){
        commit("SET_IS_LOADING", false);
        dispatch("setUserMeta", false);
      }
    }
  },
  async checkUserExists({ commit, dispatch }, email){
    console.log(email);
    try { return await this.$axios.get(`${this.getters.serverEndpoint}/user/${email}`) }
    catch { return "WRONG_EMAIL_FORMAT"; }
  },
  async createVendor({ commit, dispatch }, payload){
    return await this.$axios.post(`${this.getters.serverEndpoint}/vendor`, {
      "vendorName": payload.vendorName,
      "vendorPostcode": payload.vendorPostcode,
      "vendorDescription": payload.vendorDescription,
      "vendorLogo": payload.vendorLogo
    });
  },
  async requestPasswordReset({ commit, dispatch }, email){
    try { await this.$axios.post(`http://localhost:1337/request-password-reset`, { email }) }
    catch (e) { console.log(e) }
  },
  async register({ commit, dispatch }, payload){
    commit("SET_IS_LOADING", true);
    try {
      await this.$axios.post(`${this.getters.serverEndpoint}/register`, { "email": payload.email, "password": payload.password, "name": payload.name });
      await dispatch("login", { "email": payload.email, "password": payload.password });
      //let aData = { fromName: "Superyagi accounts", fromEmail: "<noreply@superyagi.com>", toEmail: payload.email, toName: payload.name, subject: "Thanks for signing up", html: `<h1>Welcome aboard, ${(payload.name).split(" ")[0]}!</h1><p>Cheers for signing up!</p><p>Some additional marketing fluff here.</p><p>Why don't you <a href="localhost:3000/explore">go shopping?</a></p><h4>All the best, the team at Superyagi</h4>` };
      //this.$axios.post("https://vohzd.com/email/sender.php", aData);
    }
    catch (e){
      commit("SET_IS_LOADING", false);
    }
  },
  async login({ commit, dispatch }, payload){
    commit("SET_IS_LOADING", true);
    try {
      let req = await this.$axios.post(`${this.getters.serverEndpoint}/login`, { email: payload.email, password: payload.password }, { withCredentials: true });

      console.log("LOGIN RESPONSE");
      console.log(req);
      console.log(req.data.message);
      if (req.status === 401){
        console.log("wrong password");
      }

      if (req.status === 200){
        dispatch("setUserMeta", req.data);
      }

      commit("SET_IS_LOADING", false);
    }
    catch (e){
      if (e.response.status === 401){
        commit("SET_LOGIN_NOTIFICATION", "Wrong Password.");
      }
      commit("SET_IS_LOADING", false);
    }
  },
  async logout({ commit, dispatch }){
    commit("SET_IS_LOADING", true);
    try {
      let req = await this.$axios.post(`${this.getters.serverEndpoint}/logout`, {}, { withCredentials: true });
      if (req.status === 200){
        dispatch("setNotification", "Logged out");
        dispatch("setUserMeta", null);
        commit("SET_IS_LOADING", false);
        this.$router.push("/");
      }

    }
    catch (e){
      commit("SET_IS_LOADING", false);
    }
  },
  getUserOrders({ commit }){
    this.$axios.get(`${this.getters.serverEndpoint}/orders/${this.getters.userMeta._id}`).then((res) => {
      commit("SET_USER_ORDERS", res.data.orders);
    }).catch((err) => {
      console.log(err)
    })
  },
  async getAllUsers({ commit }){
    let users = await this.$axios.get(`${this.getters.serverEndpoint}/users/`);
    commit("SET_ALL_USERS", users.data.users);
  },
  setUserMeta({ commit }, meta){
    commit("SET_USER_META", meta);
  },
  updateUserProfile({ commit }, payload){
    this.$axios.post(`${this.getters.serverEndpoint}/updateUserProfile/`, {
      "id": this.getters.userMeta._id,
      "vendorId": payload.vendorId,
      "vendorName": payload.vendorName
    }).then((res) => {
      if (res.data.success){
        commit("SET_USER_META", {
          ...this.getters.userMeta,
          "vendorId": payload.vendorId,
          "vendorName": payload.vendorName
        });
      }
    }).catch((err) => {
      console.log(err)
    })
  }
}
