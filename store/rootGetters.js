import state from "./rootState.js";

export default {
  isFirstRun: (state) => state.isFirstRun,
  isLoading: (state) => state.isLoading,
  serverEndpoint: () =>  "http://localhost:1337",
  //serverEndpoint: () =>  "https://badluckcollective.herokuapp.com"
}
